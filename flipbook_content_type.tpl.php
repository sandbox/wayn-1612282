<?php
/**
 * @file
 * The output of flipbook.
 */
?>
<div id="header_wrapper"><div id="header"></div></div>
<div id="content_wrapper">
  <div id="content-bg"><div id="content-bg-center"></div></div>
  <div id="content">
    <div class="flipbook-wrapper">
      <div class="flipbook-preloader"></div>
      <!--*************************************************************** start flipbook-->
      <div class="flipbook">
        <?php foreach ($nodes as $node) : ?>
          <div class="div_page">
            <?php print $node->body['und'][0]['safe_value']; ?>
            <img src="<?php print file_create_url($node->field_flip_image['und'][0]['uri']) ?>">
          </div>
        <?php endforeach; ?>
      </div>
      <!--*************************************************************** end flipbook-->
    </div>
    <?php if (variable_get('flipbook_block_control_enabled', 1)) : ?>
      <!--Flipbook Menu -->
      <div class="flipbook_menu">
        <a onclick="jQuery('.flipbook').flipbook.gotoPrev();">
          <div class="flipbook_arrow_left">prev</div>
        </a>
        <a onclick="jQuery('.flipbook').flipbook.gotoNext();">
          <div class="flipbook_arrow_right">next</div>
        </a>
        <a id="flipbook_btn_play" onclick="jQuery('.flipbook').flipbook.playSlideshow(); document.getElementById('flipbook_btn_play').style.display='none';  document.getElementById('flipbook_btn_pause').style.display='inline-block';">
          <div class="flipbook_btn_play">play</div>
        </a>
        <a id="flipbook_btn_pause" style="display: none;" onclick="jQuery('.flipbook').flipbook.pauseSlideshow(); document.getElementById('flipbook_btn_play').style.display='inline-block';  document.getElementById('flipbook_btn_pause').style.display='none';">
          <div class="flipbook_btn_pause">pause</div>
        </a>
        <a onclick="jQuery('.flipbook').flipbook.zoom();">
          <div class="flipbook_btn_zoom">zoom</div>
        </a>
      </div>
    <?php endif; ?>
    <!--end menu-->
    <?php if (variable_get('flipbook_block_audio_enabled', 1)) : ?>
      <audio class="flip-sound">
        <source src="<?php print drupal_get_path('module', 'flipbook') ?>/audio/flip-sound.mp3">
        <source src="<?php print drupal_get_path('module', 'flipbook') ?>/audio/flip-sound.ogg">
        Your browser does not support the audio element.
        </source>
        </source>
      </audio>
    <?php endif; ?>
  </div>
</div>
<!--end content-->
<script type="text/javascript">
  jQuery('.flipbook').css('opacity',0);

  eval(function(p,a,c,k,e,r){e=function(c){return c.toString(a)};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p
  }('0(2).4(5(){6(0.7.8&&2.9!="a"){b(c.d,e);f}0(\'.3\').3({g:h,i:j,k:"1",l:m,n:\'1\',o:\'1\'})})',25,25,'jQuery|on|document|flipbook|ready|function|if|browser|safari|readyState|complete|setTimeout|arguments|callee|120|return|totalWidth|<?php print variable_get('flipbook_block_width', 750); ?>|totalHeight|<?php print variable_get('flipbook_block_height', 500); ?>|coverPage|shadowWidth|20|settings_flip_sound|miscAction'.split('|'),0,{}))
</script>
