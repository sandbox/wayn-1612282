Flipbook module show nodes as flip style. Flipbook can only use in block, 
and provider some simple settings like control bar, 
enable default audio on flipping (only support on html5 browsers) and 
width&height.

Demo on drupal7: http://demo.webonn.com/drupal7/node/2

Demo with styled flipbook: http://demo.webonn.com/flipbook/

Flipbook module will create a content type named flipbook automatic on 
intalling add delete after uninstall.
So you just need install the module before using it.

Usage
------------
1.Create content node as flipbook type
2.Input title but we will not show it
3.Write content flipbook supports contents, flash, videos .etc
4.Go to block page and configure flipbook where you want to see it
